# Angular State Management Comparison

Comparison application for state management solutions in Angular.
The master branch will not have a working application, because it contains no state management solution.
Checkout one of the other branches and run with `ng serve` to have an application running on `http://localhost:4200/`.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5.
